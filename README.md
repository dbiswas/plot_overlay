# Plot overlay
This is a simple demonstration of how to overlay a new result on an existing image.

The image used in this example is taken from this PhD dissertation[[1]](#1).

## Reference
<a id="1">[1]</a> 
Zeißner, Sonja Verena (2021),
Development and calibration of an s-tagging algorithm and its application to constrain the CKM matrix elements |Vts| and |Vtd| in top-quark decays using ATLAS Run-2 Data,
http://dx.doi.org/10.17877/DE290R-22226
